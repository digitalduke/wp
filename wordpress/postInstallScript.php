<?php

namespace digitalduke;

class postInstallScript {
  function __construct () {
    copy("wordpress/index.php", "web/index.php");
    copy("wordpress/wp/wp-config.php", "web/wp/wp-config.php");
    mkdir("web/config");
    copy("wordpress/config/application.php", "web/config/application.php");
    $this->deleteFiles("web/wp/license.txt");
    $this->deleteFiles("web/wp/wp-config-sample.php");
    $this->deleteFiles("web/wp/composer.json");
    $this->deleteFiles("web/wp/readme.html");
    $this->deleteFiles("web/wp/wp-content/");
  }

  private function deleteFiles($target) {
    if(is_dir($target)){
      $files = array_merge( glob( $target . "*", GLOB_MARK ), glob( $target . ".*" ) );
      foreach( $files as $file ) {
        if ( substr($file, -1) !== "." ) {
          $this->deleteFiles( $file );
        }
      }
      rmdir( $target );
    } else {
      unlink( $target );
    }
  }
}

new postInstallScript;
