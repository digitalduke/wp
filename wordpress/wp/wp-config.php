<?php
/**
 * The base configuration for WordPress
 * @package WordPress
 */

define('DB_CHARSET', 'utf8mb4');
define('DB_COLLATE', '');

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

require_once( dirname(__FILE__, 2) . '/config/config-slot.php');
require_once( dirname(__FILE__, 2) . '/config/application.php');
/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
